#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import numpy as np
import pandas as pd
from utils import *
from populate_db import populate_db
from uso_obra_enum import FuzzyMatcher

# init fuzzy matcher
matcher = FuzzyMatcher()

data = acquire_data(dev_only=False, cache=False)

# some preprocessing to make list of dataframes into one
data = preprocess(data)

# minor cleanups, normalization, etc.
data = post_processing(data)

# now we need to build and cleanup each table

### arquiteto table: required fields are "nome", "nome_meio" and "sobrenome"
arq_df = pd.DataFrame({'nome': np.concatenate([data.autor.unique(), data.arquiteto_reforma.unique()])})
arq_df['key'] = arq_df['nome']

# derive middle and last name
arq_df.loc[:, 'nome'] = arq_df["nome"].str.split(' ')
arq_df = arq_df.loc[arq_df['key'] != 'Vazio', :]

arq_df["nome_meio"] = arq_df['nome'].map(lambda x: ' '.join(x[1:-1]))
arq_df["sobrenome"] = arq_df['nome'].map(lambda x: x[-1])
arq_df["nome"] = arq_df['nome'].map(lambda x: x[0])
arq_df.drop_duplicates(inplace=True)

# drop rows that have empty required fields
arq_df = select_valid_rows(arq_df, ['nome', 'sobrenome'])
arq_df.replace(['vazio', '', ' '], np.nan, inplace=True)
arq_df = arq_df.loc[arq_df['nome'].str.lower() != 'vazio', :]
arq_df['id'] = list(range(1, len(arq_df) + 1))

# escritorio
uniques = data.escritorio.unique()
uniques = uniques[uniques != 'vazio']
esq_df = pd.DataFrame({'id': list(range(1, len(uniques)+1)), 'nome': uniques})

# escritorio obra
esq_obra_df = pd.merge(
    left=esq_df.loc[:, ['nome', 'id']],
    right=data.loc[:, ['obra_id', 'escritorio']],
    left_on='nome',
    right_on='escritorio'
).loc[:, ['id', 'obra_id']]
esq_obra_df.rename(columns={'id': 'escritorio_id'}, inplace=True)
esq_obra_df['id'] = list(range(1, len(esq_obra_df)+1))

### arq obra
arq_obra_df = pd.merge(
    left=arq_df.loc[:, ['key', 'id']],
    right=data.loc[:, ['obra_id', 'autor']],
    left_on='key',
    right_on='autor'
).loc[:, ['id', 'obra_id']]
arq_obra_df.rename(columns={'id': 'arquiteto_id'}, inplace=True)
arq_obra_df['id'] = list(range(1, len(arq_obra_df)+1))

### construtora table
constr_df = pd.DataFrame({'nome': data.construtora.unique()})
constr_df = constr_df.loc[constr_df['nome'] != 'vazio', :]
constr_df['id'] = [x for x in range(1, len(constr_df)+1)]

### endereco table
end_df = data.loc[
    :,
    ['logradouro', 'numero', 'municipio', 'endereco_tipo', 'endereco_titulo', 'complemento', 'cep']
].drop_duplicates()

# drop rows that have empty required fields
end_df = select_valid_rows(end_df, ['logradouro', 'numero', 'municipio', 'endereco_tipo', 'cep'])
for col in ['endereco_tipo', 'endereco_titulo']:
    end_df.loc[:, col] = end_df.loc[:, col].apply(remove_special_characters).str.upper()
    end_df.loc[:, col] = end_df.loc[:, col].apply(lambda x: matcher.match(x, col))


end_df.replace(['vazio', 'VAZIO', '', ' '], np.nan, inplace=True)
end_df['cep'] = end_df['cep'].str.replace('-', '')
end_df['id'] = [x for x in range(1, len(end_df)+1)]

### referencias table
ref_df = data.loc[:, ['obra_id', 'referencias']].drop_duplicates()
ref_df = ref_df.loc[ref_df['referencias'] != 'vazio', :]
ref_df.rename(columns={'referencias': 'url'}, inplace=True)
ref_df['id'] = list(range(1, len(ref_df)+1))

### obra table
obra_df = data.loc[
    :,
    ["obra_id",
    "latitude",
    "longitude",
    "construtora",#"construtora_id",
    "nome_oficial",
    "ano_do_projeto",
    "ano_construcao",
    "condephaat",
    "conpresp",
    "iphan",
    "uso_original",
    "codigo_original",
    "uso_atual",
    "codigo_atual",
    "status",
    "cep",
    "logradouro",
    "numero",#"endereco_id",
    #"validado_professora",
    #"validado_dph",
    "escritorio",
    "nome_alternativo",
    "data_uso_atual",
    "ano_demolicao",
    "ano_restauro",
    "arquiteto_reforma", #"arquiteto_reforma_id",
    "ano_reforma"]
]

# treat obra status
obra_df.loc[:, 'status'] = (
    obra_df.loc[:, 'status']
        .apply(remove_special_characters).str.upper().apply(lambda x: matcher.match(x, 'obra_status'))
)

# treat uso atual / uso original using map
obra_df.loc[:, 'uso_original'] = obra_df.loc[:, 'uso_original'].str.upper()
obra_df.loc[:, 'uso_atual'] = obra_df.loc[:, 'uso_atual'].str.upper()
obra_df.loc[:, 'codigo_original'] = obra_df.loc[:, 'codigo_original'].str.upper()
obra_df.loc[:, 'codigo_atual'] = obra_df.loc[:, 'codigo_atual'].str.upper()

obra_df.loc[obra_df['uso_original'] != 'VAZIO', 'codigo_original'] = (
    obra_df.loc[obra_df['uso_original'] != 'VAZIO','uso_original'].apply(lambda x: matcher.match(x, 'uso_obra'))
)
obra_df.loc[obra_df['uso_atual'] != 'VAZIO', 'codigo_atual'] = (
    obra_df.loc[obra_df['uso_atual'] != 'VAZIO','uso_atual'].apply(lambda x: matcher.match(x, 'uso_obra'))
)

# treat lat long

# Remove minutes and seconds characters from latitude and longitude
obra_df['latitude'] = obra_df['latitude'].astype(str).str.replace(r'[°\'"]', '', regex=True)
obra_df['longitude'] = obra_df['longitude'].astype(str).str.replace(r'[°\'"]', '', regex=True)
obra_df['latitude'] = obra_df['latitude'].apply(lambda x: str(x).replace(',', '.'))
obra_df['longitude'] = obra_df['longitude'].apply(lambda x: str(x).replace(',', '.'))
obra_df = select_valid_rows(obra_df, ['latitude', 'longitude', 'nome_oficial'])
obra_df.loc[:, ['latitude', 'longitude']] = obra_df.loc[:, ['latitude', 'longitude']].replace('vazio', np.nan)

# Convert to numeric (errors='coerce' will turn any remaining non-numeric values to NaN)
obra_df['latitude'] = pd.to_numeric(obra_df['latitude'])
obra_df['longitude'] = pd.to_numeric(obra_df['longitude'])
obra_df['cep'] = obra_df['cep'].str.replace('-', '')

# Replace "vazio" with NaN
obra_df.replace(['vazio', 'VAZIO'], np.nan, inplace=True)
# remove duplicates
obra_df.drop_duplicates(inplace=True)

obra_df.rename(
    columns={'obra_id': 'id', 'ano_do_projeto': 'ano_projeto'},
    inplace=True
)

# get reference to other tables
# construtora_id
obra_df['construtora_id'] = pd.merge(
    obra_df.loc[:,'construtora'],
    constr_df,
    left_on='construtora', right_on='nome',
    how='left')['id']
obra_df.drop(columns=['construtora'], inplace=True)

#"endereco_id"
obra_df['endereco_id'] = pd.merge(
    obra_df.loc[:,['cep', 'logradouro', 'numero']],
    end_df,
    left_on=['cep', 'logradouro', 'numero'], right_on=['cep', 'logradouro', 'numero'],
    how='left')['id']
obra_df.drop(columns=['cep', 'logradouro', 'numero'], inplace=True)
# filter out invalid/null addresses
obra_df = obra_df.loc[~obra_df['endereco_id'].isna(), :]

# colunas de validação
obra_df.loc[:, ['validado_professora', 'validado_dph']] = 0

#"arquiteto_reforma_id"
obra_df['arquiteto_reforma_id'] = pd.merge(
    obra_df.loc[:,'arquiteto_reforma'],
    arq_df,
    left_on='arquiteto_reforma', right_on='key',
    how='left')['id']
obra_df.drop(columns=['arquiteto_reforma'], inplace=True)

tmp_dict = {
    'arquiteto': arq_df,
    'arquiteto_obra':arq_obra_df,
    'escritorio': esq_df,
    'escritorio_obra': esq_obra_df,
    'construtora': constr_df,
    'endereco': end_df,
    'obra': obra_df,
    'referencia': ref_df,
    'raw_obras': data
}

table_dict = {}

# delete rows that don't have references in obra_df
for key, val in tmp_dict.items():
    if 'obra_id' in val.columns:
        tmp = tmp_dict[key].copy()
        tmp = tmp.loc[tmp['obra_id'].isin(tmp_dict['obra']['id']), :]
        table_dict[key], val = tmp, tmp
    else:
        table_dict[key] = val
del tmp_dict

# save to disk
os.makedirs('tabelas_criadas', exist_ok=True)
for name, table in table_dict.items():
    table.to_csv(f'tabelas_criadas/{name}.csv', index=False)


# save to db
populate_db(table_dict)
