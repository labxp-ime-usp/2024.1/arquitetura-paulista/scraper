#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Utils for scraping process

@author: eduardokapp
"""
import os
import unicodedata
from typing import List
import pandas as pd


def select_valid_rows(df, cols):
    return df.loc[~(df[cols] == 'vazio').any(axis=1), :]

def acquire_data(dev_only=False, cache=True):
    '''
    Returns raw google sheets file as a dataframe

    Parameters
    ----------
    dev_only : bool, optional
        Flag to use or not the sheets with keyword "em desenvolvimento".
        The default is False.
    cache: bool, optional, default True
        If true, re-uses already stored tables when possible.

    Returns
    -------
    dfs : TYPE
        DESCRIPTION.

    '''
    os.makedirs('raw_data', exist_ok=True)
    if cache and len(os.listdir('raw_data')) > 0:
        dfs = []
        for file in os.listdir('raw_data'):
            dfs.append(pd.read_excel(f'raw_data/{file}'))
        return dfs

    # remove any cached files
    for file in os.listdir('raw_data'):
        os.remove(f'raw_data/{file}')

    data = pd.read_excel(
        'https://docs.google.com/spreadsheets/d/1PaWgQR1oUQlvbEPzB1vpJUCyYv2oE1Xt-szFrbuxrf4/export?format=xlsx',
        sheet_name=None
    )

    if not dev_only:
        for key in list(data.keys()):
            if 'desenvolvimento' in key or 'avulso' in key or 'antigo' in key or 'Controlado' in key or 'Padrão' in key:
                del data[key]


    dfs = list(data.values())
    for idx, df in enumerate(dfs):
        df.to_excel(f'raw_data/raw_tab_{idx}.xlsx', index=False)


    return dfs

def remove_special_characters(text: str) -> str:
    '''
    Turns non-ascii characters into ascii.

    Example:
        >>> remove_special_characters('tradução')
        >>> 'traducao'

        >>> remove_special_characters('líquido demarrado no chão àspero')
        >>> 'liquido demarrado no chao aspero'

    Parameters
    ----------
    text : str
        any text.

    Returns
    -------
    str
        "fixed" text.

    '''
    # Normalize the text to decomposed form
    normalized_text = unicodedata.normalize('NFD', text)
    # Filter out characters that are not combining characters
    filtered_text = ''.join(
        char for char in normalized_text if not unicodedata.combining(char)
    )
    # Encode the filtered text into ASCII and decode it back to string
    ascii_text = filtered_text.encode('ascii', 'ignore').decode('ascii')
    return ascii_text


def standardize_col_names(col_names: List[str]) -> None:
    '''
    Adjust basic column naming issues inplace.

    Parameters
    ----------
    col_names : List[str]
        df.columns object

    Returns
    -------
    None.

    '''
    tmp_cols = list(col_names)
    count = 0
    for idx, val in enumerate(col_names):
        tmp = val
        if not isinstance(tmp, str):
            tmp = f'unnamed_{count}'
            tmp_cols[idx] = tmp
            continue

        # remove leading, trailing whitespace
        tmp = val.lstrip().rstrip()
        # turn remaining whitespace into underscore "_"
        tmp = tmp.replace(" ", "_")
        # remove any kind of special characters
        tmp = remove_special_characters(tmp)
        # make lowercase
        tmp = tmp.lower()

        # special cases
        if 'titude' in tmp and 'http' not in tmp:
            tmp = 'latitude'
        elif ('gitude' in tmp or tmp == 'log') and 'http' not in tmp:
            tmp = 'longitude'
        elif tmp == '' or tmp == 'nan':
            tmp = f'unnamed_{count}'
        elif 'autor' in tmp or 'author' in tmp:
            tmp = 'autor'
        elif 'ano_const' in tmp:
            tmp = 'ano_construcao'
        elif 'ano_do_proj' in tmp:
            tmp = 'ano_do_projeto'

        tmp_cols[idx] = tmp
        count += 1
    return tmp_cols


def custom_df_transpose(df: pd.DataFrame, kind: int) -> pd.DataFrame:
    '''
    Very custom logic to make all tables the same structure.

    logic Kind 1: simply transpose the table and use the "nome do campo" as
    column names.

    logic kind 2: column "autor"

    Parameters
    ----------
    df : pd.DataFrame
        DESCRIPTION.
    kind : int
        Simple way of .

    Returns
    -------
    Transpose dataframe

    '''
    if kind == 1:
        # transpose
        df = df.T
        # make first row to be the column names
        df.columns = df.iloc[0]
        # drop created index
        df.reset_index(drop=True, inplace=True)
        # drop first row altogether
        return df.iloc[1::]
    elif kind == 2:
        # make the "author" column the index
        df.index = df['autor']
        # transpose
        df = df.T
        # use the index as author names, ignoring the first row
        df['author_name'] = [df.index.values[1]] + list(df.index.values[1::])
        # drop created index
        df.reset_index(drop=True, inplace=True)
        df.rename(columns={'author_name': 'autor'}, inplace=True)
        # drop first row altogether
        return df.iloc[1::]
    else:
        return None


def preprocess(dfs: List[pd.DataFrame]) -> List[pd.DataFrame]:
    '''
    Logic that standardizes all datasets.


    Parameters
    ----------
    dfs : List[pd.DataFrame]
        Every dataframe is some of the excel sheets in the arq_paulista
        docs.

    Returns
    -------
    dfs : List[pd.DataFrame]
        The list of dfs fixed.

    '''
    # fix dataframes individually first.
    for idx, val in enumerate(dfs):
        tmp = val.copy()
        # fix column names first
        tmp.columns = standardize_col_names(tmp.columns)

        # if "nome_do_campo" is a column, we need to transpose the matrix
        if "nome_do_campo" in tmp.columns:
            tmp = custom_df_transpose(tmp, 1)
            tmp.columns = standardize_col_names(tmp.columns)

        # if a standard expected column is not on the column names, there's
        # also a need to transpose.
        if not any(
            'lat' in col for col in tmp.columns if isinstance(col, str)
        ):
            tmp = custom_df_transpose(tmp, 2)
            tmp.columns = standardize_col_names(tmp.columns)

        if 'nome_oficial' in tmp.columns:
            tmp['obra'] = tmp['nome_oficial']

        # Get duplicate column names
        duplicates = tmp.columns[tmp.columns.duplicated()].unique()

        for col in duplicates:
            counter = 1
            new_cols = []
            for i, original_col in enumerate(tmp.columns):
                if original_col == col:
                    new_cols.append(f"{col}_{counter}")
                    counter += 1
                else:
                    new_cols.append(original_col)
            tmp.columns = new_cols

        # check if any of the "sem_nome_definido" look like a refs column
        for idx_c, col in enumerate(list(tmp.columns)):
            # edge case where a column is named as a reference that is applied
            # to all obras
            if 'http' in col or 'https' in col:
                tmp[col] = col
                tmp.rename(columns={col: f'referencias_{idx}'}, inplace=True)
                continue
            if 'sem_nome_definido' not in col:
                continue
            # dumb check for URLs
            if any(
                [
                    'http' in x or 'https' in x
                    for x in tmp[col].values if isinstance(x, str)
                ]
            ):
                tmp.rename(columns={col: f'referencias_{idx}'}, inplace=True)

        # sort columns
        cols = list(tmp.columns)
        cols.sort()
        tmp = tmp.loc[:, cols]
        tmp.reset_index(drop=True, inplace=True)
        tmp.fillna('vazio', inplace=True)
        dfs[idx] = tmp

    # now merge all dfs into one

    # find common col_set
    col_set = set()
    for idx, df in enumerate(dfs):
        for col in df.columns:
            col_set.add(col)

    # replace np.nans with 'vazio' to make it easier for string comparison
    for idx, df in enumerate(dfs):
        for col in col_set:
            if col not in df.columns:
                df.loc[:, col] = 'vazio'

    # merge everything
    data = pd.concat(dfs, ignore_index=True)

    # merge referencias columns into one
    data['referencias'] = data.loc[
        :, [col for col in data.columns if 'referen' in col]
    ].apply(
        lambda x: max(x, key=len),
        axis=1
    ).values

    data.drop(
        columns=[col for col in data.columns if 'referencias_' in col],
        inplace=True
    )

    return data


def post_processing(data):

    # 1. for all elements except those in the lat, long, codigo_original or
    # codigo atual columns, replace any r".d+" by whitespace
    for col in list(data.columns):
        if 'lat' in col or 'long' in col or col in ['codigo_original', 'codigo_atual']:
            continue
        data[col] = data[col].astype(str).str.replace(r'\.\d+$', '', regex=True)
        data[col] = data[col].astype(str).str.replace(r'_', ' ', regex=False)

    # remove rows where author is unnamed or unexpected
    data = data.loc[
        ~data['autor'].apply(
            lambda x: ('unnamed' in x or
                       'Sobrenome' in x)
        ),
        :
    ]

    # normalizing author names:
    # 1. left and right strip, then capitalize
    data.loc[:, 'autor'] = data.autor.str.lstrip().str.rstrip()
    data.loc[:, 'arquiteto_reforma'] = data.arquiteto_reforma.str.lstrip().str.rstrip()


    # # remove special chars
    # data.loc[:, 'autor'] = data['autor'].astype(str).str.replace(r'[^A-Za-z0-9À-ÖØ-öø-ÿ\s]', '', regex=True)
    # data.loc[:, 'arquiteto_reforma'] = data['arquiteto_reforma'].astype(str).str.replace(r'[^A-Za-z0-9À-ÖØ-öø-ÿ\s]', '', regex=True)
    # # normalize spaces
    # data.loc[:, 'autor'] = data['autor'].str.replace(r'\s+', ' ', regex=True)
    # data.loc[:, 'arquiteto_reforma'] = data['arquiteto_reforma'].str.replace(r'\s+', ' ', regex=True)

    # again, remove invalid remaning authors
    data = data.loc[data['autor'] != '', :]
    data = data.loc[data['arquiteto_reforma'] != '', :]

    # result will be a list of individual authors
    data.loc[:, 'autor'] = data['autor'].str.split(', ')
    data.loc[:, 'arquiteto_reforma'] = data['arquiteto_reforma'].str.split(', ')

    data.reset_index(drop=False, inplace=True)
    data.rename(columns={'index': 'obra_id'}, inplace=True)
    data['obra_id'] += 1
    # need to keep obra id
    data = data.explode('autor')

    # last name refinement
    data.loc[:, 'autor'] = data.autor.str.lstrip().str.rstrip().str.title()

    # explode by arquiteto reforma
    data.loc[:, 'arquiteto_reforma'] = data.loc[:, 'arquiteto_reforma'].apply(lambda x: x[0])

    # last name refinement
    data.loc[:, 'arquiteto_reforma'] = data.arquiteto_reforma.str.lstrip().str.rstrip().str.title()

    data = data.loc[data['verificado_estagiario'].str.lower() == 'sim', :]
    return data
