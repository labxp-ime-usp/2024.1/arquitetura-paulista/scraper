#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun  3 17:32:20 2024

@author: eduardokapp
"""
from sqlalchemy import create_engine, text
from sqlalchemy.engine import URL
import pandas as pd

def populate_db(data):
    print('\n\n')
    # Database Connection Details
    db_host = 'localhost'
    db_port = 3306
    db_user = input('[PROMPT] DB username?\n')
    db_pwd = input('[PROMPT] DB password?\n')
    db_name = 'cap'

    # SQLAlchemy Connection String
    connection_string = URL.create(
        drivername="mysql+pymysql",
        username=db_user,
        password=db_pwd,
        host=db_host,
        port=db_port,
        database=db_name
    )
    engine = create_engine(connection_string)

    # helper

    # Function to write row by row
    def write_row_by_row(df, engine, table_name, idx_label=None, verbose=False):
        success = 0
        for index, row in df.iterrows():
            try:
                row_df = pd.DataFrame([row])
                row_df.to_sql(name=table_name, con=engine, if_exists='append', index=False, index_label=idx_label)
                success += 1
            except Exception as e:
                if verbose:
                    start_idx = str(e).find('Error) (')
                    end_idx = str(e).find('[SQL: INSERT')
                    err_msg = str(e)[start_idx:end_idx]
                    print(f"Error writing row {index}: {err_msg}")
                else:
                    pass

        print(f'[INFO] Success rate writing df to {table_name} = {100*success/len(df)} %')


    # drop tables first
    should_continue = input(
'''This script will delete all the rows from the following tables: \n
 'construtora',\n 'arquiteto_obra',\n 'escritorio_obra'',\n 'referencia',\n 'obra',\n
 'arquiteto',\n 'endereco',\n 'escritorio'\n\nContinue? (Y/N)\n
''')
    if should_continue not in ['Y', 'y', 'yes']:
        print('Stopping script.\n')
        return None

    delete_order = [
        'construtora',
        'arquiteto_obra',
        'escritorio_obra',
        'referencia',
        'obra',
        'arquiteto',
        'endereco',
        'escritorio',
    ]
    with engine.begin() as connection:
        for tab in delete_order:
            print(f'deleted from {tab}')
            tmp = connection.execute(text(f'delete from {tab}'))


    # populate order:
    # independent tables
    # obra table
    # obra-dependant tables


    # Populate arquiteto table
    df = data['arquiteto'].loc[:, ['id', 'nome', 'nome_meio', 'sobrenome']]
    write_row_by_row(df, engine, 'arquiteto', idx_label='id')

    # construtora
    df = data['construtora'].loc[:, ['id', 'nome']]
    write_row_by_row(df, engine, 'construtora', idx_label='id')

    # endereco
    df = data['endereco']
    write_row_by_row(df, engine, 'endereco', idx_label='id')

    # escritorio
    df = data['escritorio']
    write_row_by_row(df, engine, 'escritorio', idx_label='id')

    # obras
    df = data['obra']
    write_row_by_row(df, engine, 'obra', idx_label='id')

    # arquiteto_obra
    df = data['arquiteto_obra']
    write_row_by_row(df, engine, 'arquiteto_obra',idx_label='id')

    # arquiteto_obra
    df = data['escritorio_obra']
    write_row_by_row(df, engine, 'escritorio_obra',idx_label='id')

    # referencias
    df = data['referencia']
    write_row_by_row(df, engine, 'referencia', idx_label='id')

    print('[OK] Done.\n')
