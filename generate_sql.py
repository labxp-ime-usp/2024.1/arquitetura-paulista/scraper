import os
import re


def concatenate_ordered_sql_files(folder_path, output_file):
    """Concatenates SQL files in a folder, ordered by their V-prefix."""

    # List all files in the folder
    sql_files = [f for f in os.listdir(folder_path) if f.endswith('.sql')]

    # Extract version numbers and sort
    version_pattern = re.compile(r"^V(\d+)__")
    sorted_files = sorted(
        sql_files,
        key=lambda f: int(version_pattern.match(f).group(1))  # Sort by extracted version
    )

    # Concatenate file contents
    with open(output_file, 'w') as outfile:
        for file in sorted_files:
            try:
                with open(os.path.join(folder_path, file), 'r') as infile:
                    outfile.write(infile.read())
                    outfile.write('\n')  # Add a newline for separation
            except FileNotFoundError:
                print(f"Warning: File '{file}' not found.")
            except Exception as e:
                print(f"Error processing '{file}': {e}")

# --- Example Usage ---
folder_path = "./"  # Replace with your actual folder path
output_file = "concatenated_sql.sql"     # Name of the output file

concatenate_ordered_sql_files(folder_path, output_file)
