# Visão Geral
Este repositório contém scripts para padronizar e unificar as múltiplas tabelas
contidas na [base de dados do CRCAP](https://docs.google.com/spreadsheets/d/1PaWgQR1oUQlvbEPzB1vpJUCyYv2oE1Xt-szFrbuxrf4/edit?usp=sharing).

A base é essencialmente um conjunto de abas em um Google Sheets. As abas
não possuem exatamente um padrão, algumas representam obras de um determinado
autor (leia-se: arquiteto ou escritório) enquanto outras representam obras em
uma determinada rua.

Além disso, nem todas as abas possuem os mesmos nomes ou quantidade de colunas.

# Solução
Para tentar minimizar trabalho manual, foi feito o seguinte:

* Lê-se cada aba da base de dados como uma tabela a parte.

* Normaliza-se o nome das colunas transformando tudo para minúsculo, removendo
quaisquer whitespaces e transformando caracteres com acentos para sua versão
sem acento (por ex: a frase "líquido demarrado no chão àspero" vira
"liquido demarrado no chao aspero").

* Algumas tabelas estão no formato *long* (características das obras nas colunas),
enquanto outras estão no formato *wide* (características nas linhas). Todas as
tabelas foram transformadas para long quando necessário, utilizando a coluna
"nome do campo" ou similar.

* Foi aplicada uma limpeza arbitrária nos nomes das colunas para tentar
uniformizar. Por exemplo: "latitude" e "lat" ou "referência" e "referencias".

* Para tornar o conjunto de tabelas uniforme, definimos um *set* de colunas
possíveis percorrendo todos os nomes de colunas disponíveis entre as tabelas.
Caso uma tabela não possua uma das colunas do set, criamos uma coluna vazia
de mesmo nome.

* Por fim: o resultado é separado nas múltiplas tabelas do backend.

# Como utilizar
Para reproduzir:

1. Clone este repositório.

2. Utilize Python 3.9

3. Instale libs requisito utilizando um gerenciador de pacotes python (pip, conda).

Exemplo com pip:
```
pip install -r requirements.txt
```

4. Execute o scraper com o comando:

```
python scraper.py
```

5. No terminal, siga as instruções passando user e pwd do banco de dados.
