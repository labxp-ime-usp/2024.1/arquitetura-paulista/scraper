#!/bin/bash

# Check for Python 3.9
python3.9 --version &> /dev/null
if [ $? -ne 0 ]; then
  echo "Python 3.9 not found. Installing..."

  # Install Python 3.9 based on your OS
  if [[ -x "$(command -v apt-get)" ]]; then # Debian/Ubuntu
    sudo apt-get update
    sudo apt-get install -y python3.9 python3.9-distutils
  elif [[ -x "$(command -v yum)" ]]; then    # CentOS/RHEL
    sudo yum install -y python39 python39-devel
  elif [[ -x "$(command -v dnf)" ]]; then    # Fedora
    sudo dnf install -y python39 python39-devel
  elif [[ -x "$(command -v brew)" ]]; then   # macOS (with Homebrew)
    brew install python@3.9
  else
    echo "Unsupported package manager. Please install Python 3.9 manually."
    exit 1
  fi
fi

# Upgrade pip if needed
python3.9 -m pip install --upgrade pip

# Create a virtual environment (recommended)
python3.9 -m venv scraper_env
source scraper_env/bin/activate

# Install requirements
pip install -r requirements.txt

# Run your Python script
python3.9 scraper.py
